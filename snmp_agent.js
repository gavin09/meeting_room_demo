var snmp = require("net-snmp");
var polling = require('async-polling');

var oids = ["1.3.6.1.4.1.8691.10.2210.10.1.1.4.0"];
var session;
var door_status;

exports.door_connect = function(ip) {
    session = snmp.createSession(ip, "public");

    polling(function (end) {
        // Do whatever you want.
        session.get(oids, function(error, varbinds) {
            if (error) {
                console.log(error);
                return;
            } else {
                console.log (varbinds[0].oid + " = " + varbinds[0].value);
                door_status = varbinds[0].value;
            }
        });

        end();
    }, 1000).run();
};


exports.door_status = function() {
    return door_status;
};

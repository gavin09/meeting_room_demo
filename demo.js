var express = require('express');
var snmp_agent = require('./snmp_agent.js')
var polling = require('async-polling');
var a601_door_status;
var app = express();

snmp_agent.door_connect("192.168.127.254");
app.use('/images', express.static('images'));
app.use('/static', express.static('static'));

var api = express.Router();
api.route('/meeting_rooms')
    .get(function(req, res) {
        res.json({a601:'a601'});
    });
api.route('/meeting_rooms/:room_id')
    .get(function(req, res) {
        if(req.params.room_id == "a601") {
            res.json({door: a601_door_status});
        }
    });

app.use('/api', api);

app.get('/', function(req, res) {
    res.sendFile(__dirname + '/static/demo.html');
});

app.listen(8080, function() {
    console.log('listening on port 8080');
});

polling(function (end) {
    // Do whatever you want.
    a601_door_status = snmp_agent.door_status();

    // Then notify the polling when your job is done:
    end();
    // This will schedule the next call.
}, 100).run();
